<?php
// Hide errors for better protection
error_reporting(~E_ALL);
ini_set('display_errors', 0);


// Init
$db 		= NULL;		
$groups 	= array();
$grouplist	= array();	// ID list to show chaining of groups
$items 		= array();
$maxdepth	= 0;		// The maximum depth of groups to use in the table layout

	
// Include configuration
include "functions.php";	


// Protect against corrupted config
If (is_writeable("config.php")) { 
	die("Protect the config.php file with readonly permissions before running this application."); 
}

	
// Open database connection
include "config.php";
$db = dbConnect($db_config);


// Execute user request
switch( getvar("a"))
{
	case "addGroup":
		if (getvar("name")) {
			if ($error = addGroup(array("name" => getvar("name"), "group_id" => getvar("group_id")))) {
				terminate($error);
			}
		}
		showGroups();
		break;
		
	case "addItem":
		if (getvar("name")) {
			if ($error = addItem(array("name" => getvar("name"), "group_id" => getvar("group_id")))) {
				terminate($error);
			}
		}
		showGroups();
		break;
		
	case "showItem":
		if (getvar("i")) { 
			showItem(getvar("i"));
		}
		break;
		
	default:
		showGroups();
		break;
}
terminate("");

?>