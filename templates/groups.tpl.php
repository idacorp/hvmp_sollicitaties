
<h2><a href="index.php">Testopdracht HVMP - MDP de Clerck</a></h2>
<?php

?>
Groepen en items
<br /><br />
<table>
<?php
foreach($groups as $group) {
	if ($group["group_id"] == 0) {
		showGroupRow($group["id"]); 
	}
}

?>
</table>

<br /><br />

<form action="index.php?a=addGroup" method="POST" accept-charset="UTF-8">
<b>Voeg een nieuwe groep toe : </b>
Voor de groep <select name="group_id">
<option value="0">(Geen hoofdgroep)</option>
<?php
foreach($groups as $entry) {
	?>
	<option value="<?php echo $entry["id"];?>"><?php echo $entry["reference"];?></option>
	<?php
}
?>
</select> 
met de naam <input type="text" name="name" value="">
<input type="submit" name="submit_group" value="Voeg toe">
<br />
</form>
<br /><br />

<form action="index.php?a=addItem" method="POST" accept-charset="UTF-8">
<b>Voeg een nieuw item toe : </b>
Voor de groep <select name="group_id">
<option value="0">(Geen hoofdgroep)</option>
<?php
foreach($groups as $entry) {
	?>
	<option value="<?php echo $entry["id"];?>"><?php echo $entry["reference"];?></option>
	<?php
}
?>
</select>
met de naam <input type="text" name="name" value="">
<input type="submit" name="submit_item" value="Voeg toe">
<br />
</form>

