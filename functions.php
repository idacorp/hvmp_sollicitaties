<?php


// --------------------
// A few common functions to adjust behaviour
// --------------------

function getvar($name)
{
	$value = "";
	if (isset($_GET[$name]))
		{ $value = $_GET[$name]; }
	elseif (isset($_POST[$name]))
		{ $value = $_POST[$name]; }
		
	// Sanitize input
	$value = trim($value);
	$value = strip_tags($value);
	$value = filter_var($value, FILTER_SANITIZE_STRING);
	return $value;
}


function terminate($msg)
{
	global $db;
		
	if ($db) { dbClose(); }
	die($msg);
}



// --------------------
// A few database functions to allow easy switch between mysql and mysqli
// --------------------
function dbConnect($params)
{
	$mysqli = new mysqli($params["host"], $params["username"], $params["password"], $params["database"]);

	/* check connection */
	if (mysqli_connect_errno()) {
		terminate("Connect failed: ". mysqli_connect_error());
		exit();
	}

	/* change character set to utf8 */
	if (!$mysqli->set_charset("utf8")) {
		terminate("Error loading character set utf8: ", $mysqli->error);
	} 
	return $mysqli;
}

function dbGetRow($query)
{
	global $db;
	
	if ($result = $db->query($query)) {
		$row = $result->fetch_assoc();
		$result->free();
		return $row;
	}
}


function dbGetAssoc($query)
{	
	global $db;
	
	$rows = array();
	if ($result = $db->query($query)) {
		while ($row = $result->fetch_assoc()) { 
			$rows[] = $row; 
		}
		$result->free();
	}
	return $rows;
}

function dbInsert($query)
{
	global $db;
	
	if (!$db->query($query)) {
		return $db->error;
	} 
}


function dbClose()
{
	global $db;
	
	$db->close();
}


function dbPrepare($value)
{
	global $db;
	
	if (get_magic_quotes_gpc()) { $value = stripslashes($value); }
	$value = $db->escape_string($value);
	return $value;
}


function dbUnprepare($value)
{
	$value = stripslashes($value);
	return $value;
}



// --------------------
// Group functions
// --------------------

// Return errortext on failure
function addGroup($params)
{
	$name = dbPrepare($params["name"]);
	$group_id = dbPrepare($params["group_id"]);
	$query = "INSERT INTO `".TABLE_GROUPS."` (`name`,`group_id`) VALUES ('$name','$group_id')";
	if ($result = dbInsert($query))
		{ return $result; }
}


// Obtain the groups for use in the template
function getGroups()
{	
	global $groups;
	
	// Get a total list of groups 
	$query = "SELECT * FROM `".TABLE_GROUPS."` ORDER BY `name` ";
	$rows = dbGetAssoc($query);

	$groups = array();
	foreach($rows as $row) {
		$groups[$row["id"]] = array("id" => $row["id"], "name" => $row["name"], "group_id" => $row["group_id"]);
		$groups[$row["id"]]["reference"] = getGroupReference($row["id"]);
	}

	
	// Obtain the items 
	$query = "SELECT * FROM `".TABLE_ITEMS."` ORDER BY `group_id` ";
	$rows = dbGetAssoc($query);
	

	// Reuse the group list and bind the items to them
	$items = array();
	$last_group_id = "";
	foreach($rows as $row) {	
		// Bind items when all of a group is read
		if ($last_group_id != $row["group_id"]) {
			// Bind the items to the group
			if ($last_group_id > 0) {
				$groups[$last_group_id]["items"] = $items;
				$items = array();
			}
			$last_group_id = $row["group_id"];
		}
		// Reassign item data
		$items[] = array("id" => $row["id"],
						"name" => $row["name"],
						"group_id" => $row["group_id"]);
	}
	// Bind last item(s)
	if (isset($row["id"])) {
		if ($last_group_id != $row["id"]) {
			// Bind the items to the group
			if ($last_group_id > 0) {
				$groups[$last_group_id]["items"] = $items;
				$items = array();
			}
			$last_group_id = $row["id"];
		}
	}
	
	return $groups;
}



// Obtain chained grouptitles as titles for the dropdown selection boxes
function getGroupReference($group_id)
{
	global $groups, $maxdepth;
	
	$title = $groups[$group_id]["name"];
	if ($groups[$group_id]["group_id"]) {
		$title = getGroupReference($groups[$group_id]["group_id"])." - $title"; 
	}
	// Store the depth for presentation layout 
	$v = explode("-",$title);	
	$groups[$group_id]["depth"] = count($v)-1;
	$maxdepth = max($maxdepth, count($v)-1);
	return $title;
}

	
// Show the groups and items
function showGroups()
{
	global $groups;
	
	getGroups();
	include "templates/header.tpl.php";
	include "templates/groups.tpl.php";
	include "templates/footer.tpl.php";	
}

function showGroupRow($group_id)
{
	global $groups, $maxdepth, $rowclass;
	
	// Show the main group
	if ($groups[$group_id]["group_id"] == 0) {
		// Adjust the row background
		if ($rowclass != "light") { 
			$rowclass = "light"; 
		} else {
			$rowclass = "dark"; 
		}
		// Show the row
		$grouprow = $groups[$group_id];
		include "templates/group_row.tpl.php";
	}
	
	// Show subgroups in structure
	foreach($groups as $group) {
		if ($group["group_id"] == $group_id) {
			// Adjust the row background
			if ($rowclass != "light") { 
				$rowclass = "light"; 
			} else {
				$rowclass = "dark"; 
			}
			// Show sub item as row
			$grouprow = $group;
			include "templates/group_row.tpl.php";
			showGroupRow($grouprow["id"]);
		}
	}
	
}



// --------------------
// Item functions
// --------------------

// Return errortext on failure
function addItem($params)
{
	$name = dbPrepare($params["name"]);
	$group_id = dbPrepare($params["group_id"]);
	$query = "INSERT INTO `".TABLE_ITEMS."` (`name`,`group_id`) VALUES ('$name','$group_id')";
	if ($result = dbInsert($query))
		{ return $result; }
}


// Show the item detail page
function showItem($id)
{
	$id = dbPrepare($id);
	$item = dbGetRow("SELECT * FROM `".TABLE_ITEMS."` WHERE `id` = '$id' ");
	include "templates/header.tpl.php";
	if ($item["id"]) {
		include "templates/item.tpl.php";
	} else {
		echo "Incorrect item ID."; 
	}
	include "templates/footer.tpl.php";	
}


?>